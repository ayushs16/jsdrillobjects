function mapObject( obj , cb ) {
    newObject = { ... obj};
    for( const key in obj ){
        newObject[key] = ( cb(obj[key]) );
    }
    return newObject;
}

function callback( val ){
    return ( JSON.stringify(val) + " call applied");
}


module.exports = { mapObject , callback };