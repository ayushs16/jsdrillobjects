function pairs(obj) {
    let keyArray = [];

    for( const key in obj ){
        keyArray.push( `${key} : ${obj[key]}` );
    }
    return keyArray;
}

module.exports = { pairs };