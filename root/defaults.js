function defaults( obj , defaultObject ) {

    for( const key in defaultObject ){
        if(obj[key] === undefined){
            obj[key] = defaultObject[key];
        }
    }
    return obj;
}

module.exports = { defaults };