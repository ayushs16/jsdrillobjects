function values( obj ) {
    let valuesArray = [];

    for(const property in obj  ){
        valuesArray.push( obj[ property ] );
    }
    return valuesArray;
}

module.exports = { values };