function keys(obj) {
    let keyArray = [];

    for( const key in obj ){
        keyArray.push( key );
    }
    return keyArray;
}

module.exports = { keys };