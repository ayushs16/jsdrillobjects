function invert(obj) {
    let invertedObject = new Object();

    for( const key in obj ){
        invertedObject[ `${ ( obj[ key ] ).toString() }` ] = key;
    }
    return invertedObject;
}

module.exports = { invert };