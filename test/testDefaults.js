const { testObject } = require( '../index' );
const { defaults } = require( '../root/defaults' );
const defaultObject = {
    gender : 'male',
    married : false
}

const resultedObject = defaults( testObject , defaultObject );

console.log(resultedObject);