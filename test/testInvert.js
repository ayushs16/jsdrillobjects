const { testObject } = require( '../index' );
const { invert } = require( '../root/invert' );

const arr = invert(testObject);

console.log(arr);